<?php
include_once("layouts/head.php");
include_once("layouts/header.php");
?>
<div id="primary">
    <div class="container-fluid sidebar-no clearfix">
        <div class="row">
            <!-- START CONTENT -->
            <div class="content col-sm-12 clearfix" role="main">
                <section class="indexBk hidden-xs">
                    <img src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/2018WorldCup/ENTER_Preheat/bg.jpg" alt="" class="indexBk_img img-responsive">
                </section>
            </div>
        </div>
        <!-- END CONTENT -->        
    </div>
</div>
<div class="shop-by-category container-fluid col-lg-offset-2 nav vertical">
    <div class="submenu-group hidden-sm col-sm-2">
        <div class="submenu clearfix">
            <ul id="menu-shop-by-category" class="menu">
                <li id="menu-item-1745" class="bigmenu menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-children-5">
                    <a href="product-category/accessories-type/index.html">        
                        <i data-font="retinaicon-font" data-key="retina-education-learning-036" data-icon="&#57681;" style=" font-size: 19px"></i>Moda e Indumentaria
                    </a>                   
                    <div class="submenu clearfix">
                        <ul class="sub-menu clearfix">
                            <li id="menu-item-2254" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-5">
                                <a href="#">Fashion Style</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-2255" class="menu-item menu-item-type-post_type menu-item-object-po_lookbook_fash menu-item-children-0"><a href="lookbook/summer-collection/index.html">Summer Collection</a></li>
                                        <li id="menu-item-2256" class="menu-item menu-item-type-post_type menu-item-object-po_lookbook_fash menu-item-children-0"><a href="lookbook/country-look/index.html">Country Look</a></li>
                                        <li id="menu-item-2257" class="menu-item menu-item-type-post_type menu-item-object-po_lookbook_fash menu-item-children-0"><a href="lookbook/casual-collection/index.html">Casual Collection</a></li>
                                        <li id="menu-item-2258" class="menu-item menu-item-type-post_type menu-item-object-po_lookbook_fash menu-item-children-0"><a href="lookbook/spring-collection/index.html">Spring Collection</a></li>
                                        <li id="menu-item-2259" class="menu-item menu-item-type-post_type menu-item-object-po_lookbook_fash menu-item-children-0"><a href="lookbook/classic-look/index.html">Classic Look</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1747" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-children-5">
                                <a href="product-category/accessories-type/glasses/index.html">Glasses</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-2313" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">HIPSTER GLASSES</a></li>
                                        <li id="menu-item-1784" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/tortuga-glasses/index.html">Tortuga glasses</a></li>
                                        <li id="menu-item-1786" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/woman-sunglasses/index.html">Woman sunglasses</a></li>
                                        <li id="menu-item-1787" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/black-glasses/index.html">Black glasses</a></li>
                                        <li id="menu-item-1788" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/diva-glasses/index.html">Diva glasses</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1749" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-children-4 menu-item-custom-content">
                                <a href="product-category/accessories-type/watches/index.html">Watches</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-2318" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">ELEGANT WATCHES</a></li>
                                        <li id="menu-item-1789" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/gent-snk8/index.html">Gent SNK8</a></li>
                                        <li id="menu-item-1790" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/sports-snzg/index.html">Sports SNZG</a></li>
                                        <li id="menu-item-1785" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/unisex-quartz-watch/index.html">Unisex Quartz Watch</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1748" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-children-6">
                                <a href="product-category/accessories-type/shoes/index.html">Shoes</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1853" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/fashion-boots/index.html">Fashion Boots</a></li>
                                        <li id="menu-item-1854" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/fashion-shoes/index.html">Fashion Shoes</a></li>
                                        <li id="menu-item-2260" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/classic-brown/index.html">Classic Brown</a></li>
                                        <li id="menu-item-2316" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">TRAINERS</a></li>
                                        <li id="menu-item-2317" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">WELLIES</a></li>
                                        <li id="menu-item-2315" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">SLIPPERS</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-2319" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">MAN ACCESSORIES</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-2320" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">Bags, Belts &amp; Wallets</a></li>
                                        <li id="menu-item-2321" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">Cufflinks &amp; Tie Clips</a></li>
                                        <li id="menu-item-2322" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">Hats &amp; Scarves</a></li>
                                        <li id="menu-item-2323" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">Gifts &amp; Gadgets</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li id="menu-item-1746" class="bigmenu menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-children-5">
                    <a href="product-category/accessories-type/bags/index.html"> 
                        <i data-font="retinaicon-font" data-key="retina-business-042" data-icon="&#57447;" style=" font-size: 19px">

                        </i>Calzado</a>
                    <a class="custom-item-1746 custom-item-yitimage custom-item-image" href="product-category/accessories-type/bags/index.html">
                        <img src="../../i0.wp.com/live.yithemes.com/nielsen/uploads/sites/68/2014/12/bgbag10c6f.jpg?fit=689%2C410&amp;ssl=1" alt="Bags" width="689" height="410"></a>
                    <div class="submenu clearfix">
                        <ul class="sub-menu clearfix">
                            <li id="menu-item-1760" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">Mujeres</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1767" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-red-bag/index.html">Leather Red Bag</a></li>
                                        <li id="menu-item-1766" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/summer-leather/index.html">Summer leather</a></li>
                                        <li id="menu-item-1769" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/casual-bag/index.html">Lifetime Bag</a></li>
                                        <li id="menu-item-1775" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1761" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">Hombres</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1768" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/africa-bag/index.html">Africa Bag</a></li>
                                        <li id="menu-item-1770" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/crock-bag/index.html">Crock Bag</a></li>
                                        <li id="menu-item-1764" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/casual-bag/index.html">Casual Bag</a></li>
                                        <li id="menu-item-1777" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-red-bag/index.html">Elegant Red Bag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1763" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Niños</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1771" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Women Briefcase</a></li>
                                        <li id="menu-item-1772" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Leather Briefcase</a></li>
                                        <li id="menu-item-1776" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1762" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Bebe</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1765" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/man-briefcase/index.html">Man  Briefcase</a></li>
                                        <li id="menu-item-1773" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Brown Briefcase</a></li>
                                        <li id="menu-item-1774" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Business Briefcase</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li id="menu-item-1750" class="bigmenu menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-children-5">
                    <a href="product-category/electronics/index.html">
                        <i data-font="retinaicon-font" data-key="retina-gadgets-device-tech035" data-icon="&#57956;" style=" font-size: 19px">
                        </i>Hogar y Bazar</a>
                    <a class="custom-item-1750 custom-item-yitimage custom-item-image" href="product-category/electronics/index.html">
                        <img src="../../i0.wp.com/live.yithemes.com/nielsen/uploads/sites/68/2014/12/bgtech8a64.jpg?fit=642%2C422&amp;ssl=1" alt="Electronics" width="642" height="422">
                    </a>
                    <div class="submenu clearfix">
                        <ul class="sub-menu clearfix">
                            <li id="menu-item-1760" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">Cocina</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1767" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-red-bag/index.html">Leather Red Bag</a></li>
                                        <li id="menu-item-1766" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/summer-leather/index.html">Summer leather</a></li>
                                        <li id="menu-item-1769" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/casual-bag/index.html">Lifetime Bag</a></li>
                                        <li id="menu-item-1775" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1761" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">Dormitorio</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1768" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/africa-bag/index.html">Africa Bag</a></li>
                                        <li id="menu-item-1770" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/crock-bag/index.html">Crock Bag</a></li>
                                        <li id="menu-item-1764" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/casual-bag/index.html">Casual Bag</a></li>
                                        <li id="menu-item-1777" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-red-bag/index.html">Elegant Red Bag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1763" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Bano</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1771" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Women Briefcase</a></li>
                                        <li id="menu-item-1772" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Leather Briefcase</a></li>
                                        <li id="menu-item-1776" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1762" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Muebles</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1765" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/man-briefcase/index.html">Man  Briefcase</a></li>
                                        <li id="menu-item-1773" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Brown Briefcase</a></li>
                                        <li id="menu-item-1774" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Business Briefcase</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li id="menu-item-1750" class="bigmenu menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-children-5">
                    <a href="product-category/electronics/index.html">
                        <i data-font="retinaicon-font" data-key="retina-gadgets-device-tech035" data-icon="&#57956;" style=" font-size: 19px">
                        </i>Libreria y Oficina</a>
                    <a class="custom-item-1750 custom-item-yitimage custom-item-image" href="product-category/electronics/index.html">
                        <img src="../../i0.wp.com/live.yithemes.com/nielsen/uploads/sites/68/2014/12/bgtech8a64.jpg?fit=642%2C422&amp;ssl=1" alt="Electronics" width="642" height="422">
                    </a>
                    <div class="submenu clearfix">
                        <ul class="sub-menu clearfix">
                            <li id="menu-item-1760" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">Cocina</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1767" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-red-bag/index.html">Leather Red Bag</a></li>
                                        <li id="menu-item-1766" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/summer-leather/index.html">Summer leather</a></li>
                                        <li id="menu-item-1769" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/casual-bag/index.html">Lifetime Bag</a></li>
                                        <li id="menu-item-1775" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1761" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">Dormitorio</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1768" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/africa-bag/index.html">Africa Bag</a></li>
                                        <li id="menu-item-1770" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/crock-bag/index.html">Crock Bag</a></li>
                                        <li id="menu-item-1764" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/casual-bag/index.html">Casual Bag</a></li>
                                        <li id="menu-item-1777" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-red-bag/index.html">Elegant Red Bag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1763" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Bano</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1771" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Women Briefcase</a></li>
                                        <li id="menu-item-1772" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Leather Briefcase</a></li>
                                        <li id="menu-item-1776" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1762" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Muebles</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1765" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/man-briefcase/index.html">Man  Briefcase</a></li>
                                        <li id="menu-item-1773" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Brown Briefcase</a></li>
                                        <li id="menu-item-1774" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Business Briefcase</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li id="menu-item-1750" class="bigmenu menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-children-5">
                    <a href="product-category/electronics/index.html">
                        <i data-font="retinaicon-font" data-key="retina-gadgets-device-tech035" data-icon="&#57956;" style=" font-size: 19px">
                        </i>Ropa Interior</a>
                    <a class="custom-item-1750 custom-item-yitimage custom-item-image" href="product-category/electronics/index.html">
                        <img src="../../i0.wp.com/live.yithemes.com/nielsen/uploads/sites/68/2014/12/bgtech8a64.jpg?fit=642%2C422&amp;ssl=1" alt="Electronics" width="642" height="422">
                    </a>
                    <div class="submenu clearfix">
                        <ul class="sub-menu clearfix">
                            <li id="menu-item-1760" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">Mujeres</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1767" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-red-bag/index.html">Leather Red Bag</a></li>
                                        <li id="menu-item-1766" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/summer-leather/index.html">Summer leather</a></li>
                                        <li id="menu-item-1769" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/casual-bag/index.html">Lifetime Bag</a></li>
                                        <li id="menu-item-1775" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1761" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">Hombres</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1768" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/africa-bag/index.html">Africa Bag</a></li>
                                        <li id="menu-item-1770" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/crock-bag/index.html">Crock Bag</a></li>
                                        <li id="menu-item-1764" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/casual-bag/index.html">Casual Bag</a></li>
                                        <li id="menu-item-1777" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-red-bag/index.html">Elegant Red Bag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1763" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Niños</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1771" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Women Briefcase</a></li>
                                        <li id="menu-item-1772" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Leather Briefcase</a></li>
                                        <li id="menu-item-1776" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li id="menu-item-1750" class="bigmenu menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-has-children menu-item-children-5">
                    <a href="product-category/electronics/index.html">
                        <i data-font="retinaicon-font" data-key="retina-gadgets-device-tech035" data-icon="&#57956;" style=" font-size: 19px">
                        </i>Equipaje</a>
                    <a class="custom-item-1750 custom-item-yitimage custom-item-image" href="product-category/electronics/index.html">
                        <img src="../../i0.wp.com/live.yithemes.com/nielsen/uploads/sites/68/2014/12/bgtech8a64.jpg?fit=642%2C422&amp;ssl=1" alt="Electronics" width="642" height="422">
                    </a>
                    <div class="submenu clearfix">
                        <ul class="sub-menu clearfix">
                            <li id="menu-item-1760" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">Carteras</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1767" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-red-bag/index.html">Leather Red Bag</a></li>
                                        <li id="menu-item-1766" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/summer-leather/index.html">Summer leather</a></li>
                                        <li id="menu-item-1769" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/casual-bag/index.html">Lifetime Bag</a></li>
                                        <li id="menu-item-1775" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1761" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-4">
                                <a href="#">Mochilas</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1768" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/africa-bag/index.html">Africa Bag</a></li>
                                        <li id="menu-item-1770" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/crock-bag/index.html">Crock Bag</a></li>
                                        <li id="menu-item-1764" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/casual-bag/index.html">Casual Bag</a></li>
                                        <li id="menu-item-1777" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-red-bag/index.html">Elegant Red Bag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1763" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Bolsos</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1771" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Women Briefcase</a></li>
                                        <li id="menu-item-1772" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Leather Briefcase</a></li>
                                        <li id="menu-item-1776" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1763" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Bolsos</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1771" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Women Briefcase</a></li>
                                        <li id="menu-item-1772" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Leather Briefcase</a></li>
                                        <li id="menu-item-1776" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1763" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Valijas</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1771" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Women Briefcase</a></li>
                                        <li id="menu-item-1772" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Leather Briefcase</a></li>
                                        <li id="menu-item-1776" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li id="menu-item-1763" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-3">
                                <a href="#">Accesorios de Viaje</a>
                                <div class="submenu clearfix">

                                    <ul class="sub-menu clearfix">
                                        <li id="menu-item-1771" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/women-briefcase/index.html">Women Briefcase</a></li>
                                        <li id="menu-item-1772" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-briefcase/index.html">Leather Briefcase</a></li>
                                        <li id="menu-item-1776" class="menu-item menu-item-type-post_type menu-item-object-product menu-item-children-0"><a href="product/leather-handbag/index.html">Leather handbag</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>                
            </ul>
        </div>
    </div>
    <div class="clearfix submenu-group2 row">
        <section class="clearfix col-xs-12 col-sm-8 col-md-12 col-lg-6">
            <div id="slider">
                <a href="javascript:void(0)" class="control_next">></a>
                <a href="javascript:void(0)" class="control_prev"><</a>
                <ul>
                    <li>
                        <img class="img-responsive " src="https://uidesign.gbtcdn.com/GB/image/promotion/20180619_3814/680x420.jpg?imbypass=true" alt="" style="opacity: 1;">
                    </li>
                    <li>
                        <img class="img-responsive" src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/2018WorldCup/ENTER_Preheat/banner.jpg" alt="" style="opacity: 1;">
                    </li>
                    <li>
                        <img class="img-responsive" src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/2018WorldCup/LuckyWheel/Wheel680X420b.jpg?imbypass=true" alt="" style="opacity: 1;">
                    </li>
                    <li>
                        <img class="img-responsive" src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/2018WorldCup/Venue/680X420.jpg?imbypass=true" alt="" style="opacity: 1;">
                    </li>
                </ul>  
            </div>
        </section>
        <section class="indexBan_daily hidden-xs col-sm-3 col-md-4 col-lg-3">
            <div class="indexBan_dailyTitle">
                <a href="https://www.gearbest.com/gadget-deals.html">Ofertas del día</a>
                <div id="sliderVer">
                    <a href="javascript:void(0);" class="control_next">></a>
                    <a href="javascript:void(0);" class="control_prev"><</a>
                    <ul>
                        <li>
                            <a href="https://www.gearbest.com/gadget-deals.html?deal_id=40381" class="indexBan_dailyImg" tabindex="0">
                                <img src="https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2017/07/05/goods-goods_thumb_220/20170705134052_79318.jpg" class="img-responsive">
                            </a>
                            <a href="https://www.gearbest.com/gadget-deals.html?deal_id=36185" class="indexBan_dailyText" tabindex="-1">
                                MEGIR M2020 Male Quartz Watch
                                <span class="indexBan_dailyShopPrice js-currency" data-async-price="266844102#1433363" data-currency="7.99">SFr.8.11</span>
                                <span class="indexBan_dailyMarketPrice js-currency" data-currency="9.62">SFr.9.76</span>
                            </a>                    
                        </li>
                        <li>
                            <a href="https://www.gearbest.com/gadget-deals.html?deal_id=40529" class="indexBan_dailyImg" tabindex="-1">
                                <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-product-pic/Electronic/2018/06/14/source-img/20180614134439_62810.jpg" alt="" class="img-responsive">
                            </a>
                        </li>
                    </ul>  
                </div>
            </div>
        </section>
        <section class="Bansingle col-xs-12 col-sm-12 col-md-12 col-lg-9 clearfix">
            <div class="clearfix col-xs-6 col-sm-3">
                <a href="https://www.gearbest.com/promotion-cubot-power-special-2685.html" class="indexBan_single" data-track-key="7570" data-track-module="A_2">
                    <img class="img-responsive" data-lazy="https://uidesign.gbtcdn.com/GB/images/promotion/2018/aPOWER/220X150.jpg?impolicy=high" alt="" src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/aPOWER/220X150.jpg?impolicy=high" data-was-processed="true">
                </a>            
            </div>
            <div class="clearfix col-xs-6 col-sm-3">
                <a href="https://www.gearbest.com/promotion-zeblaze-Brand-Sale-special-2601.html" class="indexBan_single" data-track-key="7571" data-track-module="A_3">
                    <img class="img-responsive" data-lazy="https://uidesign.gbtcdn.com/GB/image/promotion/20180614_3778/220X150.jpg?impolicy=high" alt="" src="https://uidesign.gbtcdn.com/GB/image/promotion/20180614_3778/220X150.jpg?impolicy=high" data-was-processed="true">
                </a>            
            </div>
            <div class="clearfix col-xs-6 col-sm-3">
                <a href="https://www.gearbest.com/promotion-one-stop-shopping-special-1546.html" class="indexBan_single" data-track-key="7572" data-track-module="A_4">
                    <img class="img-responsive" data-lazy="https://uidesign.gbtcdn.com/GB/image/promotion/20180615_3801/220.jpg?imbypass=true" alt="" src="https://uidesign.gbtcdn.com/GB/image/promotion/20180615_3801/220.jpg?imbypass=true" data-was-processed="true">
                </a>
            </div>
            <div class="clearfix col-xs-6 col-sm-3">
                <a href="https://www.gearbest.com/coupon.html" class="indexBan_single" data-track-key="6636" data-track-module="A_5">
                    <img class="img-responsive" data-lazy="https://uidesign.gbtcdn.com/GB/images/index/2018/main_banner/0505/260x150.jpg?impolicy=high" alt="" src="https://uidesign.gbtcdn.com/GB/images/index/2018/main_banner/0505/260x150.jpg?impolicy=high" data-was-processed="true">
                </a>
            </div>
        </section>
    </div>
    <div class="clearfix submenu-group2 row col-xs-12 col-md-12 col-lg-10">
        <section>
            <h2>Relacionado con los artículos que has visto</h2>
            <div class="indexRecom_content">
                <div class="indexRecom_fixedBox hidden-xs hidden-sm col-md-3 col-lg-2">
                    <h3 class="indexRecom_subTitle">Visto recientemente</h3>
                    <ul class="indexRecom_fixed goodsItem-initialized">
                        <li class="gbGoodsItem gbGoodsItem-index">
                            <span class="gbGoodsItem_discount">
                                <strong class="gbGoodsItem_per">40</strong>%
                                <i class="gbGoodsItem_off">OFF</i>
                            </span>
                            <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/night-lights/pp_1671602.html?wid=1433363" data-id="259224201" data-img="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-product-pic/Electronic/2018/05/04/source-img/20180504165701_12022.jpg">
                                <img class="gbGoodsItem_image img-responsive" src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-product-pic/Electronic/2018/05/04/source-img/20180504165701_12022.jpg" >
                            </a>
                            <div class="gbGoodsItem_outBox">
                                <a class="gbGoodsItem_title" href="https://www.gearbest.com/night-lights/pp_1671602.html?wid=1433363" title="HTV - 777 Night Lamp Adapter  ">
                                    HTV - 777 Night Lamp Adapter  
                                </a>
                                <p class="gbGoodsItem_price js-currency" data-currency="2.99">SFr.3.03</p>
                                <p class="gbGoodsItem_ctrl">
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clearfix indexRecom_slick indexRecom_slick-other col-xs-12 col-md-9 col-lg-10">
                    <h3 class="indexRecom_subTitle">Otros vieron estos artículos relacionados</h3>
                    <div id="sliderVer-views-relac">
                        <a href="javascript:void(0);" class="control_next">></a>
                        <a href="javascript:void(0);" class="control_prev"><</a>
                        <ul class="indexRecom_slickCon js-recomSlick goodsItem-initialized slick-initialized slick-slider">
                            <li class="gbGoodsItem gbGoodsItem-index  ">
                                <span class="gbGoodsItem_discount">
                                    <strong class="gbGoodsItem_per">10</strong>%
                                    <i class="gbGoodsItem_off">OFF</i>
                                </span>
                                <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/ceiling-lights/pp_1830509.html?wid=1433363" >
                                    <img class="gbGoodsItem_image img-responsive" src="https://gloimg.gbtcdn.com/soa/gb/pdm-product-pic/Electronic/2018/05/04/goods-img/1525370046891389300.jpg" >
                                </a>
                                <div class="gbGoodsItem_outBox">
                                    <a class="gbGoodsItem_title" href="https://www.gearbest.com/ceiling-lights/pp_1830509.html?wid=1433363" title="Xiaomi Philips Zhirui Adjustable Color Temperature Downlight" tabindex="-1">
                                        Xiaomi Philips Zhirui Adjustable Color Temperature Downlight
                                    </a>
                                    <p class="gbGoodsItem_price js-currency" data-currency="14.99">SFr.15.21</p>
                                    <p class="gbGoodsItem_ctrl">
                                    </p>
                                </div>
                            </li>
                            <li class="gbGoodsItem gbGoodsItem-index  ">
                                <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/night-lights/pp_664631.html?wid=1433363" >
                                    <img class="img-responsive gbGoodsItem_image" src="https://gloimg.gbtcdn.com/gb/pdm-product-pic/Distribution/2018/01/16/goods-img/1516035972231275346.jpg" alt="Utorch 6 LEDs Motion Sensor Night Light" title="Utorch 6 LEDs Motion Sensor Night Light">
                                </a>
                                <div class="gbGoodsItem_outBox">
                                    <a class="gbGoodsItem_title" href="https://www.gearbest.com/night-lights/pp_664631.html?wid=1433363" title="Utorch 6 LEDs Motion Sensor Night Light" tabindex="-1">
                                        Utorch 6 LEDs Motion Sensor Night Light
                                    </a>
                                    <p class="gbGoodsItem_price js-currency" data-currency="3.32">SFr.3.37</p>
                                    <p class="gbGoodsItem_ctrl">
                                    </p>
                                </div>
                            </li>
                            <li class="gbGoodsItem gbGoodsItem-index  ">
                                <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/night-lights/pp_1628238.html?wid=1433363">
                                    <img class="img-responsive gbGoodsItem_image " src="https://gloimg.gbtcdn.com/gb/pdm-provider-img/straight-product-img/20180129/T000992/T0009920170/goods-img/1517351717475708743.jpg"  alt="Cabinet Hinge LED Light 1pc" title="Cabinet Hinge LED Light 1pc">
                                </a>
                                <div class="gbGoodsItem_outBox">
                                    <a class="gbGoodsItem_title" href="https://www.gearbest.com/night-lights/pp_1628238.html?wid=1433363" title="Cabinet Hinge LED Light 1pc" tabindex="-1">
                                        Cabinet Hinge LED Light 1pc
                                    </a>
                                    <p class="gbGoodsItem_price js-currency" data-currency="0.64">SFr.0.65</p>
                                    <p class="gbGoodsItem_ctrl">
                                    </p>
                                </div>
                            </li>
                            <li class="gbGoodsItem gbGoodsItem-index  ">
                                <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/novelty-lighting/pp_605214.html?wid=1433363">
                                    <img class="img-responsive gbGoodsItem_image" src="https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2016/12/19/goods-img/1498201862295279385.jpg" data-lazy="https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2016/12/19/goods-img/1498201862295279385.jpg" alt="10pcs Remote Control Waterproof LED Tea Light" title="10pcs Remote Control Waterproof LED Tea Light">
                                </a>
                                <div class="gbGoodsItem_outBox">
                                    <a class="gbGoodsItem_title" href="https://www.gearbest.com/novelty-lighting/pp_605214.html?wid=1433363" title="10pcs Remote Control Waterproof LED Tea Light" tabindex="-1">
                                        10pcs Remote Control Waterproof LED Tea Light
                                    </a>
                                    <p class="gbGoodsItem_price js-currency" data-currency="9.86">SFr.10.01</p>
                                    <p class="gbGoodsItem_ctrl">
                                    </p>
                                </div>
                            </li>      
                            <li class="gbGoodsItem gbGoodsItem-index  ">
                                <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/table-lamps/pp_946803.html?wid=1451237">
                                    <img class="img-responsive gbGoodsItem_image" src="https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2017/12/29/goods-img/1514513276354021078.jpg" alt="Xiaomi Yeelight YLFW01YL Smart Atmosphere Candela Light" title="Xiaomi Yeelight YLFW01YL Smart Atmosphere Candela Light">
                                </a>
                                <div class="gbGoodsItem_outBox">
                                    <a class="gbGoodsItem_title" href="https://www.gearbest.com/table-lamps/pp_946803.html?wid=1451237" title="Xiaomi Yeelight YLFW01YL Smart Atmosphere Candela Light" tabindex="0">
                                        Xiaomi Yeelight YLFW01YL Smart Atmosphere Candela Light
                                    </a>
                                    <p class="gbGoodsItem_price js-currency" data-currency="45.02">SFr.45.69</p>
                                    <p class="gbGoodsItem_ctrl">
                                    </p>
                                </div>
                            </li>
                            <li class="gbGoodsItem gbGoodsItem-index ">
                                <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/led-light-bulbs/pp_132002.html?wid=1433363">
                                    <img class="img-responsive gbGoodsItem_image " src="https://gloimg.gbtcdn.com/gb/2015/201505/goods-img/1499102062054385818.jpg" alt="L0403 Multifunctional Infrared Radial Sensor PIR Detector 4 LEDs Door Light" title="L0403 Multifunctional Infrared Radial Sensor PIR Detector 4 LEDs Door Light" data-was-processed="true" style="opacity: 1;">
                                </a>
                                <div class="gbGoodsItem_outBox">
                                    <a class="gbGoodsItem_title" href="https://www.gearbest.com/led-light-bulbs/pp_132002.html?wid=1433363" title="L0403 Multifunctional Infrared Radial Sensor PIR Detector 4 LEDs Door Light" tabindex="0">
                                        L0403 Multifunctional Infrared Radial Sensor PIR Detector 4 LEDs Door Light
                                    </a>
                                    <p class="gbGoodsItem_price js-currency" data-currency="3.99">SFr.4.05</p>
                                    <p class="gbGoodsItem_ctrl">
                                    </p>
                                </div>
                            </li>      
                            <li class="gbGoodsItem gbGoodsItem-index  " data-id="263541601">
                                <span class="gbGoodsItem_discount">
                                    <strong class="gbGoodsItem_per">50</strong>%
                                    <i class="gbGoodsItem_off">OFF</i>
                                </span>
                                <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/indoor-lights/pp_1807120.html?wid=1433363">
                                    <img class="img-responsive gbGoodsItem_image " src="https://gloimg.gbtcdn.com/soa/gb/pdm-product-pic/Electronic/2018/04/14/goods-img/1523818587021671540.jpg" alt="Utorch Cabinet Hinge LED Sensor Light" title="Utorch Cabinet Hinge LED Sensor Light" data-was-processed="true" style="opacity: 1;">
                                </a>
                                <div class="gbGoodsItem_outBox">
                                    <a class="gbGoodsItem_title" href="https://www.gearbest.com/indoor-lights/pp_1807120.html?wid=1433363" title="Utorch Cabinet Hinge LED Sensor Light" tabindex="0">
                                        Utorch Cabinet Hinge LED Sensor Light
                                    </a>
                                    <p class="gbGoodsItem_price js-currency" data-currency="0.99">SFr.1.00</p>
                                    <p class="gbGoodsItem_ctrl">
                                    </p>
                                </div>
                            </li>
                            <li class="gbGoodsItem gbGoodsItem-index  ">
                                <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/night-lights/pp_1696118.html?wid=1433363">
                                    <img class="img-responsive gbGoodsItem_image " src="https://gloimg.gbtcdn.com/soa/gb/pdm-product-pic/Electronic/2018/06/20/goods-img/1529470777736674753.jpg" alt="Mobile Power Supply USB LED Lamp Light Portable Notebook Computer for Millet" title="Mobile Power Supply USB LED Lamp Light Portable Notebook Computer for Millet" data-was-processed="true" style="opacity: 1;">
                                </a>
                                <div class="gbGoodsItem_outBox">
                                    <a class="gbGoodsItem_title" href="https://www.gearbest.com/night-lights/pp_1696118.html?wid=1433363" title="Mobile Power Supply USB LED Lamp Light Portable Notebook Computer for Millet" tabindex="0">
                                        Mobile Power Supply USB LED Lamp Light Portable Notebook Computer for Millet
                                    </a>
                                    <p class="gbGoodsItem_price js-currency" data-currency="0.54">SFr.0.55</p>
                                    <p class="gbGoodsItem_ctrl">
                                    </p>
                                </div>
                            </li>
                            <li class="gbGoodsItem gbGoodsItem-index  ">
                                <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/table-lamps/pp_363779.html?wid=1654075">
                                    <img class="img-responsive gbGoodsItem_image " src="https://gloimg.gbtcdn.com/soa/gb/pdm-product-pic/Electronic/2018/05/08/goods-img/1525751344868946999.jpg" alt="Xiaomi Mijia Yeelight MJTD01YL Smart LED Desk Lamp" title="Xiaomi Mijia Yeelight MJTD01YL Smart LED Desk Lamp" data-was-processed="true" style="opacity: 1;">
                                </a>
                                <div class="gbGoodsItem_outBox">
                                    <a class="gbGoodsItem_title" href="https://www.gearbest.com/table-lamps/pp_363779.html?wid=1654075" title="Xiaomi Mijia Yeelight MJTD01YL Smart LED Desk Lamp" tabindex="0">
                                        Xiaomi Mijia Yeelight MJTD01YL Smart LED Desk Lamp
                                    </a>
                                    <p class="gbGoodsItem_price js-currency" data-currency="35.95">SFr.36.49</p>
                                    <p class="gbGoodsItem_ctrl">
                                    </p>
                                </div>
                            </li>
                            <li class="gbGoodsItem gbGoodsItem-index  ">
                                <a class="gbGoodsItem_thumb js-selectItemd" href="https://www.gearbest.com/table-lamps/pp_715451.html?wid=1451237">
                                    <img class="img-responsive gbGoodsItem_image " src="https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2017/08/22/goods-img/1503439405902872658.jpg">
                                </a>
                                <div class="gbGoodsItem_outBox">
                                    <a class="gbGoodsItem_title" href="https://www.gearbest.com/table-lamps/pp_715451.html?wid=1451237" title="Xiaomi Yeelight YLTD01YL LED Table Light" tabindex="-1">
                                        Xiaomi Yeelight YLTD01YL LED Table Light
                                    </a>
                                    <p class="gbGoodsItem_price js-currency" data-currency="27.95">SFr.28.37</p>
                                    <p class="gbGoodsItem_ctrl">
                                    </p>
                                </div>
                            </li>
                        </ul>  
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="clearfix submenu-group2 row col-xs-12">
        <section>
            <h2 class="col-xs-12 col-md-4">Top Electrónicos</h2>
            <div class="indexLayer_sub col-xs-12 col-md-6">
                <a href="https://www.gearbest.com/promotion-cubot-power-special-2685.html" class="indexLayer_subItem" style="color: #000000">
                    <span class="tag"> new </span>
                    Cubot Power
                </a>
                <a href="https://www.gearbest.com/promotion-Alfawise-U20-special-2578.html" class="indexLayer_subItem" style="color: #000000">
                    Impresora 3D U20
                </a>
                <a href="https://www.gearbest.com/promotion-UMIDIGI-Z2-special-2554.html" class="indexLayer_subItem" style="color: #000000">
                    Umidigi Z2
                </a>
                <a href="https://www.gearbest.com/drone-_gear/?odr=hot" class="indexLayer_subItem" style="color: #000000">
                    Drone
                </a>
                <a href="https://www.gearbest.com/smart-watches/pp_009928044572.html?wid=1433363" class="indexLayer_subItem" style="color: #000000">
                    Mi Band 3
                </a>
            </div>
            <div id="sliderLayer" class="hidden-xs col-sm-7 col-lg-4 clearfix">
                <a href="javascript:void(0)" class="control_next">></a>
                <a href="javascript:void(0)" class="control_prev"><</a>
                <ul>
                    <li>
                        <div class="indexLayer_slickItem">
                            <a href="https://www.gearbest.com/promotion-cubot-power-special-2685.html" class="indexLayer_slickItemLink" tabindex="-1">
                                <img class="indexLayer_slickImg" src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/aPOWER/480X600.jpg?impolicy=high" alt="" style="opacity: 1;">
                            </a>
                            <div class="indexLayer_slickText">
                                <h3 class="indexLayer_slickTitle" style="color: #000">Cubot Power</h3>
                                <p class="indexLayer_slickDes" style="color: #000">Global First Launch From $129.99</p>
                            </div>
                            <div class="indexLayer_slickTag" style="background-color:#FF8A00 ">
                                <span class="indexLayer_slickTagText" style="color:#fff">Oferta de mitad de año</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="indexLayer_slickItem">
                            <a href="https://www.gearbest.com/promotion-cool-consumer-electronics-sale-special-513.html" class="indexLayer_slickItemLink">
                                <img class="indexLayer_slickImg" src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/aConsumer/480X600.jpg?impolicy=hight" alt="">
                            </a>
                            <div class="indexLayer_slickText">
                                <h3 class="indexLayer_slickTitle" style="color: #000">JETSET JUNE SALE</h3>
                                <p class="indexLayer_slickDes" style="color: #000">UP TO 50% OFF</p>
                            </div>
                            <div class="indexLayer_slickTag" style="background-color:#FF8A00 ">
                                <span class="indexLayer_slickTagText" style="color:#fff">Oferta de mitad de año</span>
                            </div>
                        </div>
                    </li>
                </ul>  
            </div>
            <div class="col-xs-12 col-sm-5 col-lg-4 clearfix">
                <div class="indexLayer_midleft">
                    <a class="indexLayer_midleftItem" href="https://www.gearbest.com/promotion-Alfawise-U20-special-2578.html" >
                        <img class="img-responsive" src="https://uidesign.gbtcdn.com/GB/images/others/J-GB/u201.jpg?impolicy=high" alt="">
                        <div class="indexLayer_midleftBox">
                            <h3 class="indexLayer_midleftTitle" style="color: #000">Alfawise U20 3D Printer</h3>
                            <p class="indexLayer_midleftDes" style="color: #000">En venta ahora@ $299.99</p>
                        </div>
                    </a>
                    <a class="indexLayer_midleftItem" href="https://www.gearbest.com/promotion-ASUS-zenfone-5Z-special-2570.html" data-track-key="2882">
                        <img class="img-responsive" src="https://uidesign.gbtcdn.com/GB/images/others/J-GB/ASUS.jpg?impolicy=high" alt="" >
                        <div class="indexLayer_midleftBox">
                            <h3 class="indexLayer_midleftTitle" style="color: #000">ASUS Zenfone 5Z</h3>
                            <p class="indexLayer_midleftDes" style="color: #000">Junio 18 - 25 @ $549.99</p>
                        </div>
                    </a>
                    <a class="indexLayer_midleftItem" href="https://www.gearbest.com/robot-vacuum/pp_009816316347.html?wid=1349303" >
                        <img class="img-responsive" src="https://uidesign.gbtcdn.com/GB/images/others/ivy/Robot%20Vacuum.jpg?imbypass=true" alt="" >
                        <div class="indexLayer_midleftBox">
                            <h3 class="indexLayer_midleftTitle" style="color: #000">Xiaomi Mi Robot Vacuum</h3>
                            <p class="indexLayer_midleftDes" style="color: #000">5200mAh capacidad @ $349.99</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="hidden-sm hidden-xs col-sm-2 clearfix">
                <div class="indexLayer_hot">
                    <header class="indexLayer_hotTitle">Más vendidos</header>
                    <div id="hot-slayer" class="indexLayer_hotSlick js-layerHotSlick slick-initialized slick-slider">
                        <a href="javascript:void(0)" class="control_next">></a>
                        <a href="javascript:void(0)" class="control_prev"><</a>
                        <ul class="indexLayer_hotSlickItem">
                            <li>
                                <div class="indexLayer_hotSlickItem">
                                    <div class="indexLayer_hotItem" data-track-key="174859502_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/living-appliances/pp_344666.html?wid=1433363" class="indexLayer_hotImg" tabindex="-1">
                                            <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-product-pic/Electronic/2018/05/26/source-img/20180526084212_21514.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/living-appliances/pp_344666.html?wid=1433363" class="indexLayer_hotText" tabindex="-1">Original Xiaomi Mi Smart WiFi Remote Control Gateway Upgrade</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="174859502#1433363" data-currency="15.99">SFr.16.23</span>
                                    </div>
                                    <div class="indexLayer_hotItem" data-track-key="264925301_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/sports-fitness-headphones/pp_1826692.html?wid=1433363" class="indexLayer_hotImg" tabindex="-1">
                                            <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-provider-img/straight-product-img/20180424/T017187/T0171870147/source-img/051942-1921.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/sports-fitness-headphones/pp_1826692.html?wid=1433363" class="indexLayer_hotText" tabindex="-1">Cwxuan Sports Magnetic Bluetooth V4.1 Stereo Earphone with Microphone</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="264925301#1433363" data-currency="5.86">SFr.5.95</span>
                                    </div>
                                    <div class="indexLayer_hotItem" data-track-key="226953801_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/samsung-note-series/pp_780352.html?wid=1433363" class="indexLayer_hotImg" tabindex="-1">
                                            <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-product-pic/Electronic/2018/06/11/source-img/20180611165106_52425.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/samsung-note-series/pp_780352.html?wid=1433363" class="indexLayer_hotText" tabindex="-1">Angibabe Ultra-thin Durable Full Surface Coverage PET Soft Film for Samsung Galaxy Note 8 6.3 inch</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="226953801#1433363" data-currency="1.05">SFr.1.07</span>
                                    </div>
                                    <div class="indexLayer_hotItem" data-track-key="225538004_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/tv-box/pp_736119.html?wid=1433363" class="indexLayer_hotImg" tabindex="-1">
                                            <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-product-pic/Electronic/2017/08/30/source-img/20170830101507_83759.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/tv-box/pp_736119.html?wid=1433363" class="indexLayer_hotText" tabindex="-1">W95 TV Box</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="225538004#1433363" data-currency="29.99">SFr.30.44</span>
                                    </div>
                                    <div class="indexLayer_hotItem" data-track-key="269382801_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/smart-watches/pp_009557274002.html?wid=1433363" class="indexLayer_hotImg" tabindex="-1">
                                            <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-product-pic/Electronic/2018/06/15/source-img/20180615160912_29364.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/smart-watches/pp_009557274002.html?wid=1433363" class="indexLayer_hotText" tabindex="-1">I9 Smart Bracelet</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="269382801#1433363" data-currency="11.85">SFr.12.03</span>
                                    </div>
                                    <div class="indexLayer_hotItem" data-track-key="207917503_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/tv-box-mini-pc/pp_607556.html?wid=1433363" class="indexLayer_hotImg" tabindex="-1">
                                            <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-product-pic/Electronic/2018/06/21/source-img/20180621160347_88792.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/tv-box-mini-pc/pp_607556.html?wid=1433363" class="indexLayer_hotText" tabindex="-1">Beelink GT1 Ultimate 3GB DDR4 + 32GB EMMC TV Box</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="207917503#1433363" data-currency="71.99">SFr.73.06</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="indexLayer_hotSlickItem">
                                    <div class="indexLayer_hotItem" data-track-key="179677702_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/headphone-accessories/pp_360241.html?wid=1433363" class="indexLayer_hotImg" tabindex="0">
                                            <img src="https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2016/05/10/goods-goods_thumb_220/20160510141322_61192.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/headphone-accessories/pp_360241.html?wid=1433363" class="indexLayer_hotText" tabindex="0">New Bee Lightweight Aluminum Stand for Headphones</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="179677702#1433363" data-currency="5.22">SFr.5.30</span>
                                    </div>
                                    <div class="indexLayer_hotItem" data-track-key="229757901_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/speakers/pp_977813.html?wid=1433363" class="indexLayer_hotImg" tabindex="0">
                                            <img src="https://gloimg.gbtcdn.com/gb/pdm-product-pic/Electronic/2017/10/11/goods-goods_thumb_220/1507846320756302639.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/speakers/pp_977813.html?wid=1433363" class="indexLayer_hotText" tabindex="0">YPF - 03 Bluetooth Transmitter Portable Audio Device</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="229757901#1433363" data-currency="7.89">SFr.8.01</span>
                                    </div>
                                    <div class="indexLayer_hotItem" data-track-key="121915601_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/smart-home/pp_157258.html?wid=1433363" class="indexLayer_hotImg" tabindex="0">
                                            <img src="https://gloimg.gbtcdn.com/gb/2015/201503/goods-goods_thumb_220/1425592748309-P-2445457.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/smart-home/pp_157258.html?wid=1433363" class="indexLayer_hotText" tabindex="0">LCD Carbon Monoxide Alarm Gas Detector with LED Light</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="121915601#1433363" data-currency="7.85">SFr.7.97</span>
                                    </div>
                                    <div class="indexLayer_hotItem" data-track-key="156658901_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/mice-keyboards/pp_261728.html?wid=1433363" class="indexLayer_hotImg" tabindex="0">
                                            <img src="https://gloimg.gbtcdn.com/gb/2015/201511/goods-goods_thumb_220/1520562819786022056.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/mice-keyboards/pp_261728.html?wid=1433363" class="indexLayer_hotText" tabindex="0">Classic USB Controller for SNES</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="156658901#1433363" data-currency="8.73">SFr.8.86</span>
                                    </div>
                                    <div class="indexLayer_hotItem" data-track-key="262402001_1349303" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/upright-vacuums/pp_1707089.html?wid=1349303" class="indexLayer_hotImg" tabindex="0">
                                            <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-product-pic/Electronic/2018/04/04/source-img/20180404154324_73962.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/upright-vacuums/pp_1707089.html?wid=1349303" class="indexLayer_hotText" tabindex="0">Xiaomi ROIDMI Wireless Strong Suction Vacuum Cleaner</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="262402001#1349303" data-currency="329.99">SFr.334.91</span>
                                    </div>
                                    <div class="indexLayer_hotItem" data-track-key="250338602_1433363" data-track-module="C_1">
                                        <a href="https://www.gearbest.com/stands-holders/pp_1513176.html?wid=1433363" class="indexLayer_hotImg" tabindex="0">
                                            <img src="https://gloimg.gbtcdn.com/gb/pdm-provider-img/straight-product-img/20171221/T012019/T0120190149/goods-goods_thumb_220/1514221746386708678.jpg" alt="">
                                        </a>
                                        <a href="https://www.gearbest.com/stands-holders/pp_1513176.html?wid=1433363" class="indexLayer_hotText" tabindex="0">Universal Adjustable Octopus Tripod Phone Holder for Xiaomi / Samsung / Huawei / iPhone</a>
                                        <span class="indexLayer_hotPrice js-currency js-asyncPrice" data-async-price="250338602#1433363" data-currency="2.5">SFr.2.54</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="clearfix submenu-group2 row col-xs-12">
        <section>
            <h2 class="col-xs-12 col-md-4">Estilo de vida</h2>
            <div class="indexLayer_sub col-xs-12 col-md-6">
                <a href="https://www.gearbest.com/promotion-cubot-power-special-2685.html" class="indexLayer_subItem" style="color: #000000">
                    <span class="tag"> new </span>
                    Relojes deportivos
                </a>
                <a href="https://www.gearbest.com/promotion-Alfawise-U20-special-2578.html" class="indexLayer_subItem" style="color: #000000">
                    Mata mosquitos electirco
                </a>
                <a href="https://www.gearbest.com/promotion-UMIDIGI-Z2-special-2554.html" class="indexLayer_subItem" style="color: #000000">
                    Coleccionables
                </a>
                <a href="https://www.gearbest.com/drone-_gear/?odr=hot" class="indexLayer_subItem" style="color: #000000">
                    Manualidades
                </a>
                <a href="https://www.gearbest.com/smart-watches/pp_009928044572.html?wid=1433363" class="indexLayer_subItem" style="color: #000000">
                    Iluminación
                </a>
            </div>
            <div id="sliderLayer-2" class="hidden-xs col-sm-7 col-lg-4 clearfix">
                <a href="javascript:void(0)" class="control_next">></a>
                <a href="javascript:void(0)" class="control_prev"><</a>
                <ul>
                    <li>
                        <div class="indexLayer_slickItem">
                            <a href="https://www.gearbest.com/promotion-cubot-power-special-2685.html" class="indexLayer_slickItemLink" tabindex="-1">
                                <img class="indexLayer_slickImg" src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/aPOWER/480X600.jpg?impolicy=high" alt="" style="opacity: 1;">
                            </a>
                            <div class="indexLayer_slickText">
                                <h3 class="indexLayer_slickTitle" style="color: #000">Cubot Power</h3>
                                <p class="indexLayer_slickDes" style="color: #000">Global First Launch From $129.99</p>
                            </div>
                            <div class="indexLayer_slickTag" style="background-color:#FF8A00 ">
                                <span class="indexLayer_slickTagText" style="color:#fff">Oferta de mitad de año</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="indexLayer_slickItem">
                            <a href="https://www.gearbest.com/promotion-cool-consumer-electronics-sale-special-513.html" class="indexLayer_slickItemLink">
                                <img class="indexLayer_slickImg" src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/aConsumer/480X600.jpg?impolicy=hight" alt="">
                            </a>
                            <div class="indexLayer_slickText">
                                <h3 class="indexLayer_slickTitle" style="color: #000">JETSET JUNE SALE</h3>
                                <p class="indexLayer_slickDes" style="color: #000">UP TO 50% OFF</p>
                            </div>
                            <div class="indexLayer_slickTag" style="background-color:#FF8A00 ">
                                <span class="indexLayer_slickTagText" style="color:#fff">Oferta de mitad de año</span>
                            </div>
                        </div>
                    </li>
                </ul>  
            </div>
            <div class="col-xs-12 col-sm-5 col-lg-6 clearfix">
                <div class="clearfix indexLayer_mid col-xs-6 col-md-4" data-track-key="2885" data-track-module="D_2">
                    <a href="https://www.gearbest.com/other-educational-toys/pp_009461935866.html?wid=1433363" class="indexLayer_midLink">
                        <img class="img-responsive indexLayer_midImg initial " src="https://uidesign.gbtcdn.com/GB/images/others/J-GB/waterg.jpg?impolicy=high" alt="">
                    </a>
                    <h4 class="indexLayer_midTitle">Fireman Backpack Water Gun</h4>
                    <p class="indexLayer_midDes">Compra ahora por @ $11.39</p>
                </div>
                <div class="indexLayer_mid col-xs-6 col-md-4 clearfix" data-track-key="2858" data-track-module="D_3">
                    <a href="https://www.gearbest.com/backpacks/pp_009864477677.html?wid=1433363" class="indexLayer_midLink">
                        <img class="img-responsive indexLayer_midImg initial " src="https://uidesign.gbtcdn.com/GB/images/others/J-GB/blue.jpg?impolicy=high" alt="">
                    </a>
                    <h4 class="indexLayer_midTitle">Trendy Durable Men Backpack</h4>
                    <p class="indexLayer_midDes">Promocion: $4.98</p>
                </div>    
                <div class="indexLayer_presale hidden-xs hidden-sm col-md-4 clearfix">
                    <div class="indexLayer_switch js-preSwitch">
                        <span class="indexLayer_switchItem active">Compra ahora</span>
                        <span class="indexLayer_switchItem">Pronto</span>
                    </div>
                    <div class="indexLayer_presaleBox js-indexLayer-presale">
                        <div class="presale-wrap js-presale-wrap active" style="font-size: 1px;">
                            <div class="presale-wrap-lists js-wrap-lists slick-initialized slick-slider">
                                <div class="slick-list">
                                    <div class="slick-track">
                                        <div class="slick-slide slick-current slick-active">
                                            <div>
                                                <div class="indexLayer_presale_list" data-track-key="268859401_1433363">
                                                    <div class="indexLayer_counter js-layerCounter" data-begin="1529830799" data-end="1529917199">Termina en: 11:20:21</div>
                                                    <div class="indexLayer_presaleImg" data-text="deal ended">
                                                        <a href="https://www.gearbest.com/block-toys/pp_009615006382.html?wid=1433363" class="indexLayer_presaleLink" tabindex="0">
                                                            <img class="img-responsive" src="https://gloimg.gbtcdn.com/soa/gb/pdm-product-pic/Electronic/2018/05/24/goods-img/1527214205000670181.jpg" alt="" data-was-processed="true">
                                                        </a>
                                                    </div>
                                                    <div class="presale_list_price">
                                                        <div class="list_price_now">
                                                            <span class="indexLayer_preShopPrice js-currency js-asyncPrice" data-async-price="268859401#1433363" data-currency="13.99">SFr.14.20</span>
                                                            <em class="js-currency js-asyncPrice indexBan_dailyMarketPrice asyncPrice-no" data-currency="16.98">SFr.17.23</em>
                                                        </div>
                                                        <span class="presale_list_num"> <strong>97</strong> Productos </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="presale-wrap js-presale-wrap">
                            <div class="presale-wrap-lists js-wrap-lists slick-initialized slick-slider">
                                <div class="slick-list">
                                    <div class="slick-track">
                                        <div class="slick-slide slick-current slick-active">
                                            <div>
                                                <div class="indexLayer_presale_list">
                                                    <div class="indexLayer_counter js-layerCounter">Starts In: 11:20:21</div>
                                                    <div class="indexLayer_presaleImg">
                                                        <a href="https://www.gearbest.com/novelty-toys/pp_1848098.html?wid=1433363" class="indexLayer_presaleLink" tabindex="0">
                                                            <img class="img-responsive" src="https://gloimg.gbtcdn.com/soa/gb/pdm-product-pic/Electronic/2018/06/01/goods-img/1527814640214735237.jpg" alt="" data-was-processed="true">
                                                        </a>
                                                    </div>
                                                    <div class="presale_list_price">
                                                        <div class="list_price_now">
                                                            <span class="indexLayer_preShopPrice js-currency js-asyncPrice" data-async-price="266789701#1433363" data-currency="4.99">SFr.5.06</span>
                                                            <em class="js-currency js-asyncPrice indexBan_dailyMarketPrice asyncPrice-no" data-currency="6.89">SFr.6.99</em>
                                                        </div>
                                                        <span class="presale_list_num"> <strong>99</strong> productos </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="presale-wrap js-presale-wrap ">
                            <div class="presale-wrap-lists js-wrap-lists slick-initialized slick-slider">
                                <div class="slick-list">
                                    <div class="slick-track">
                                        <div class="slick-slide slick-current slick-active">
                                            <div>
                                                <div class="indexLayer_presale_list" data-track-key="268643301_1433363">
                                                    <div class="indexLayer_counter js-layerCounter" data-begin="1530003599" data-end="1530089999">Ends In: 0:0:0</div>
                                                    <div class="indexLayer_presaleImg" data-text="deal ended">
                                                        <a href="https://www.gearbest.com/novelty-lighting/pp_009844373731.html?wid=1433363" class="indexLayer_presaleLink" tabindex="0">
                                                            <img class="img-responsive" src="https://gloimg.gbtcdn.com/soa/gb/pdm-provider-img/straight-product-img/20180522/T014623/T0146230385/goods-img/1526943018401644732.jpg" alt="" data-was-processed="true">
                                                        </a>
                                                    </div>
                                                    <div class="presale_list_price">
                                                        <div class="list_price_now">
                                                            <span class="indexLayer_preShopPrice">SFr.10.40</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="indexLayer_list" class="clearfix indexLayer_list colxs-12 col-md-7 col-lg-12">
                    <a href="javascript:void(0)" class="control_next">></a>
                    <a href="javascript:void(0)" class="control_prev"><</a>
                    <ul class="slick-track">
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4 col-xs-6">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/gb/pdm-provider-img/straight-product-img/20171228/T016724/T0167240201/goods-goods_thumb_220/1514938494759393917.jpg">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/gb/pdm-product-pic/Distribution/2017/02/17/goods-goods_thumb_220/20170217151404_96717.jpg" alt="" style="opacity: 1;">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/gb/pdm-provider-img/straight-product-img/20171228/T016724/T0167240201/goods-goods_thumb_220/1514938494759393917.jpg">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-product-pic/Electronic/2018/05/04/source-img/20180504165701_12022.jpg" alt="" style="opacity: 1;">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/gb/pdm-provider-img/straight-product-img/20171228/T016724/T0167240201/goods-goods_thumb_220/1514938494759393917.jpg">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/gb/pdm-provider-img/straight-product-img/20171228/T016724/T0167240201/goods-goods_thumb_220/1514938494759393917.jpg">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </section>
    </div>
    <div class="clearfix submenu-group2 row col-xs-12">
        <section>
            <h2 class="col-xs-12 col-md-4">Cool Gadgets</h2>
            <div class="indexLayer_sub col-xs-12 col-md-6">
                <a href="https://www.gearbest.com/promotion-cubot-power-special-2685.html" class="indexLayer_subItem" style="color: #000000">
                    $0.99 oferta
                </a>
                <a href="https://www.gearbest.com/promotion-Alfawise-U20-special-2578.html" class="indexLayer_subItem" style="color: #000000">
                    Accesorios Apple
                </a>
                <a href="https://www.gearbest.com/promotion-UMIDIGI-Z2-special-2554.html" class="indexLayer_subItem" style="color: #000000">
                    Juguetes
                </a>
                <a href="https://www.gearbest.com/drone-_gear/?odr=hot" class="indexLayer_subItem" style="color: #000000">
                    Cocina
                </a>
            </div>
            <div id="sliderLayer-3" class="hidden-xs col-sm-7 col-lg-4 clearfix">
                <a href="javascript:void(0)" class="control_next">></a>
                <a href="javascript:void(0)" class="control_prev"><</a>
                <ul>
                    <li>
                        <div class="indexLayer_slickItem">
                            <a href="https://www.gearbest.com/promotion-cubot-power-special-2685.html" class="indexLayer_slickItemLink" tabindex="-1">
                                <img class="indexLayer_slickImg" src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/diy/480x600.jpg?impolicy=hight" alt="" style="opacity: 1;">
                            </a>
                            <div class="indexLayer_slickText">
                                <h3 class="indexLayer_slickTitle" style="color: #000">Cubot Power</h3>
                                <p class="indexLayer_slickDes" style="color: #000">Global First Launch From $129.99</p>
                            </div>
                            <div class="indexLayer_slickTag" style="background-color:#FF8A00 ">
                                <span class="indexLayer_slickTagText" style="color:#fff">Oferta de mitad de año</span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="indexLayer_slickItem">
                            <a href="https://www.gearbest.com/promotion-cool-consumer-electronics-sale-special-513.html" class="indexLayer_slickItemLink">
                                <img class="indexLayer_slickImg" src="https://uidesign.gbtcdn.com/GB/images/promotion/2018/aConsumer/480X600.jpg?impolicy=hight" alt="">
                            </a>
                            <div class="indexLayer_slickText">
                                <h3 class="indexLayer_slickTitle" style="color: #000">JETSET JUNE SALE</h3>
                                <p class="indexLayer_slickDes" style="color: #000">UP TO 50% OFF</p>
                            </div>
                            <div class="indexLayer_slickTag" style="background-color:#FF8A00 ">
                                <span class="indexLayer_slickTagText" style="color:#fff">Oferta de mitad de año</span>
                            </div>
                        </div>
                    </li>
                </ul>  
            </div>
            <div class="col-xs-12 col-sm-5 col-lg-6 clearfix">
                <div class="clearfix indexLayer_mid col-xs-6 col-md-4" data-track-key="2885" data-track-module="D_2">
                    <a href="https://www.gearbest.com/other-educational-toys/pp_009461935866.html?wid=1433363" class="indexLayer_midLink">
                        <img class="img-responsive indexLayer_midImg initial " src="https://uidesign.gbtcdn.com/GB/images/others/top_brands/270724501-620.jpg" alt="" data-lazy="https://uidesign.gbtcdn.com/GB/images/others/top_brands/270724501-620.jpg" data-was-processed="true">
                    </a>
                    <h4 class="indexLayer_midTitle">Fireman Backpack Water Gun</h4>
                    <p class="indexLayer_midDes">Compra ahora por @ $11.39</p>
                </div>
                <div class="indexLayer_mid col-xs-6 col-md-4 clearfix" data-track-key="2858" data-track-module="D_3">
                    <a href="https://www.gearbest.com/backpacks/pp_009864477677.html?wid=1433363" class="indexLayer_midLink">
                        <img class="img-responsive indexLayer_midImg initial " src="https://uidesign.gbtcdn.com/GB/images/others/J-GB/blue.jpg?impolicy=high" alt="" data-lazy="https://uidesign.gbtcdn.com/GB/images/others/J-GB/blue.jpg?impolicy=high" data-was-processed="true">
                    </a>
                    <h4 class="indexLayer_midTitle">Trendy Durable Men Backpack</h4>
                    <p class="indexLayer_midDes">Promocion: $4.98</p>
                </div>    
                <div class="indexLayer_presale hidden-xs hidden-sm col-md-4 clearfix">
                    <div class="indexLayer_switch js-preSwitch">
                        <span class="indexLayer_switchItem active">Compra ahora</span>
                        <span class="indexLayer_switchItem">Pronto</span>
                    </div>
                    <div class="indexLayer_presaleBox js-indexLayer-presale">
                        <div class="presale-wrap js-presale-wrap active" style="font-size: 1px;">
                            <div class="presale-wrap-lists js-wrap-lists slick-initialized slick-slider">
                                <div class="slick-list">
                                    <div class="slick-track">
                                        <div class="slick-slide slick-current slick-active">
                                            <div>
                                                <div class="indexLayer_presale_list" data-track-key="268859401_1433363">
                                                    <div class="indexLayer_counter js-layerCounter" data-begin="1529830799" data-end="1529917199">Termina en: 11:20:21</div>
                                                    <div class="indexLayer_presaleImg" data-text="deal ended">
                                                        <a href="https://www.gearbest.com/block-toys/pp_009615006382.html?wid=1433363" class="indexLayer_presaleLink" tabindex="0">
                                                            <img class="img-responsive" src="https://gloimg.gbtcdn.com/soa/gb/pdm-product-pic/Electronic/2018/05/24/goods-img/1527214205000670181.jpg" alt="" data-was-processed="true">
                                                        </a>
                                                    </div>
                                                    <div class="presale_list_price">
                                                        <div class="list_price_now">
                                                            <span class="indexLayer_preShopPrice js-currency js-asyncPrice" data-async-price="268859401#1433363" data-currency="13.99">SFr.14.20</span>
                                                            <em class="js-currency js-asyncPrice indexBan_dailyMarketPrice asyncPrice-no" data-currency="16.98">SFr.17.23</em>
                                                        </div>
                                                        <span class="presale_list_num"> <strong>97</strong> Productos </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="presale-wrap js-presale-wrap">
                            <div class="presale-wrap-lists js-wrap-lists slick-initialized slick-slider">
                                <div class="slick-list">
                                    <div class="slick-track">
                                        <div class="slick-slide slick-current slick-active">
                                            <div>
                                                <div class="indexLayer_presale_list">
                                                    <div class="indexLayer_counter js-layerCounter">Starts In: 11:20:21</div>
                                                    <div class="indexLayer_presaleImg">
                                                        <a href="https://www.gearbest.com/novelty-toys/pp_1848098.html?wid=1433363" class="indexLayer_presaleLink" tabindex="0">
                                                            <img class="img-responsive" src="https://gloimg.gbtcdn.com/soa/gb/pdm-product-pic/Electronic/2018/06/01/goods-img/1527814640214735237.jpg" alt="" data-was-processed="true">
                                                        </a>
                                                    </div>
                                                    <div class="presale_list_price">
                                                        <div class="list_price_now">
                                                            <span class="indexLayer_preShopPrice js-currency js-asyncPrice" data-async-price="266789701#1433363" data-currency="4.99">SFr.5.06</span>
                                                            <em class="js-currency js-asyncPrice indexBan_dailyMarketPrice asyncPrice-no" data-currency="6.89">SFr.6.99</em>
                                                        </div>
                                                        <span class="presale_list_num"> <strong>99</strong> productos </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="presale-wrap js-presale-wrap ">
                            <div class="presale-wrap-lists js-wrap-lists slick-initialized slick-slider">
                                <div class="slick-list">
                                    <div class="slick-track">
                                        <div class="slick-slide slick-current slick-active">
                                            <div>
                                                <div class="indexLayer_presale_list" data-track-key="268643301_1433363">
                                                    <div class="indexLayer_counter js-layerCounter" data-begin="1530003599" data-end="1530089999">Ends In: 0:0:0</div>
                                                    <div class="indexLayer_presaleImg" data-text="deal ended">
                                                        <a href="https://www.gearbest.com/novelty-lighting/pp_009844373731.html?wid=1433363" class="indexLayer_presaleLink" tabindex="0">
                                                            <img class="img-responsive" src="https://gloimg.gbtcdn.com/soa/gb/pdm-provider-img/straight-product-img/20180522/T014623/T0146230385/goods-img/1526943018401644732.jpg" alt="" data-was-processed="true">
                                                        </a>
                                                    </div>
                                                    <div class="presale_list_price">
                                                        <div class="list_price_now">
                                                            <span class="indexLayer_preShopPrice">SFr.10.40</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="indexLayer_list-2" class="indexLayer_list colxs-12">
                    <a href="javascript:void(0)" class="control_next">></a>
                    <a href="javascript:void(0)" class="control_prev"><</a>
                    <ul class="slick-track">
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/gb/pdm-provider-img/straight-product-img/20171221/T012019/T0120190149/goods-goods_thumb_220/1514221746386708678.jpg" alt="" style="opacity: 1;">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/gb/pdm-provider-img/straight-product-img/20171228/T016724/T0167240201/goods-goods_thumb_220/1514938494759393917.jpg">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-provider-img/straight-product-img/20180426/T019096/T0190960285/source-img/172147-3363.jpg" alt="" style="opacity: 1;">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/gb/pdm-provider-img/straight-product-img/20171228/T016724/T0167240201/goods-goods_thumb_220/1514938494759393917.jpg">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/soa/gb/thumb-extend/pdm-provider-img/straight-product-img/20180426/T019096/T0190960285/source-img/172147-3363.jpg" alt="" style="opacity: 1;">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>
                        <li class="slick-slide slick-cloned col-xs-6 col-lg-4">
                            <div class="indexLayer_listItem">
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listImg">
                                    <img src="https://gloimg.gbtcdn.com/gb/pdm-provider-img/straight-product-img/20171228/T016724/T0167240201/goods-goods_thumb_220/1514938494759393917.jpg">
                                </a>
                                <a href="https://www.gearbest.com/men-s-watches/pp_1535318.html?wid=1433363" class="indexLayer_listTitle" tabindex="-1">Mens Fashion Sports Watch</a>
                                <span class="indexLayer_listPrice js-currency js-asyncPrice" data-async-price="251138001#1433363" data-currency="2.86">SFr.2.90</span>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </section>
    </div>
    <div class="clearfix submenu-group2 row col-xs-12">
        <div class="indexAvow col-xs-12 col-md-10">
            <h4 class="indexAvow_header">COMPRAS EN LÍNEA EN GEARBEST.COM: UNA MAYOR SELECCIÓN DE PRODUCTOS DE CALIDAD A LOS PRECIOS AL POR MAYOR EN LÍNEA</h4>
            <p class="indexAvow_des">Nuestra amplia y asequible gama incluye lo último en electrónica y gadgets, incluidos teléfonos inteligentes, tabletas, relojes inteligentes, cámaras de acción, televisores, televisores, drones, impresoras 3D, dvr para automóviles, junto con los últimos juguetes geniales como scooters, accesorios para juegos, muñecas casas, juegos de simulación y productos de estilo de vida de alta calidad que comprenden aspiradoras, purificadores de aire, utensilios de cocina, luces de techo, linterna, pintura al óleo, etc.
            </p>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
    function slider(xnombre) {
    let slideCount = $('#' + xnombre + ' ul li').length;
            let slideWidth = $('#' + xnombre + ' ul li').width();
            let slideHeight = $('#' + xnombre + ' ul li').height();
            $('#' + xnombre).css({height: slideHeight});
            $('#' + xnombre + ' ul li:last-child').prependTo('#' + xnombre + ' ul');
            function moveLeft() {
            $('#' + xnombre + ' ul').animate({
            left: + slideWidth
            }, 200, function () {
            $('#' + xnombre + ' ul li:last-child').prependTo('#' + xnombre + ' ul');
                    $('#' + xnombre + ' ul').css('left', '');
            });
            }
    ;
            function moveRight() {
            $('#' + xnombre + ' ul').animate({
            left: - slideWidth
            }, 200, function () {
            $('#' + xnombre + ' ul li:first-child').appendTo('#' + xnombre + ' ul');
                    $('#' + xnombre + ' ul').css('left', '');
            });
            }
    ;
            $('#' + xnombre + ' a.control_prev').click(function () {
    moveLeft();
    });
            $('#' + xnombre + ' a.control_next').click(function () {
    moveRight();
    });
    }
    slider("slider");
            slider("sliderVer");
            slider("sliderVer-views-relac");
            slider("sliderLayer");
            slider("hot-slayer");
            slider("sliderLayer-2");
            slider("indexLayer_list");
            slider("sliderLayer-3");
            slider("indexLayer_list-2");
    });
</script>
<?php
include_once("layouts/footer.php");
