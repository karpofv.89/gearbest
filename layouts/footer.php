
<div id="footer-copyright-group">
    <!-- START FOOTER -->
    <div class="clear"></div>
    <div id="footer">
        <div class="container">
            <div class="border">
                <div class="row">
                    <div class="col-sm-4 col-md-push-8"><div class="row"><div id="yit-newsletter-form-3" class="widget col-sm-12 newsletter-form">
                                <h3>ENJOY NIELSEN</h3>
                                <p class="newsletter-subtitle">Register now to get updates on promotions and coupons.</p>
                                <div class="newsletter-section clearfix newsletter-widget">
                                    <p class="description">
                                    </p>

                                    <form method="post" action="#">
                                        <fieldset><ul class="group">
                                                <li>
                                                    <label for="email">Please enter your mail address</label>
                                                    <div class="newsletter_form_email">
                                                        <input type="text" placeholder="Please enter your mail address" name="email" id="email" class="email-field text-field  autoclear">
                                                    </div>
                                                </li>
                                                <li>
                                                    <input type="submit" class="btn submit-field btn-flat-orange btn-large" value="SUBSCRIBE">
                                                </li>
                                            </ul></fieldset>
                                    </form>
                                </div>

                            </div></div></div>

                    <div class="col-sm-8 col-md-pull-4">
                        <div class="row">

                            <div class="footer-row-1 footer-columns-4">
                                <div id="nav_menu-3" class="widget col-xs-6 col-sm-3 widget_nav_menu">
                                    <h3>Stay Connected</h3>
                                    <div class="menu-stay-connected-container"><ul id="menu-stay-connected" class="menu">
                                            <li id="menu-item-1864" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1864"><a href="#">Facebook</a></li>
                                            <li id="menu-item-1865" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1865"><a href="#">YouTube</a></li>
                                            <li id="menu-item-1866" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1866"><a href="#">Twitter</a></li>
                                            <li id="menu-item-1868" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1868"><a href="#">Google+</a></li>
                                            <li id="menu-item-1869" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1869"><a href="#">Our Community</a></li>
                                            <li id="menu-item-1870" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1870"><a href="#">Leave a Feedback</a></li>
                                        </ul></div>
                                </div>
                                <div id="nav_menu-4" class="widget col-xs-6 col-sm-3 widget_nav_menu">
                                    <h3>How To Buy</h3>
                                    <div class="menu-how-to-buy-container"><ul id="menu-how-to-buy" class="menu">
                                            <li id="menu-item-1883" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1883"><a href="#">Create an Account</a></li>
                                            <li id="menu-item-1884" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1884"><a href="#">Making Payments</a></li>
                                            <li id="menu-item-1885" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1885"><a href="#">Delivery Options</a></li>
                                            <li id="menu-item-1886" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1886"><a href="#">Buyer Protection</a></li>
                                            <li id="menu-item-1887" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1887"><a href="#">Terms and conditions</a></li>
                                        </ul></div>
                                </div>
                                <div id="nav_menu-5" class="widget col-xs-6 col-sm-3 widget_nav_menu">
                                    <h3>Customer Service</h3>
                                    <div class="menu-customer-service-container"><ul id="menu-customer-service" class="menu">
                                            <li id="menu-item-1876" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1876"><a href="#">Contact Us</a></li>
                                            <li id="menu-item-1877" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1877"><a href="#">Our FAQ</a></li>
                                            <li id="menu-item-1878" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1878"><a href="#">Ask Our community</a></li>
                                            <li id="menu-item-1879" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1879"><a href="#">Affiliate Program</a></li>
                                            <li id="menu-item-1881" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1881"><a href="#">Transaction Agreement</a></li>
                                            <li id="menu-item-1882" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1882"><a href="#">Recruiting Test Participants</a></li>
                                        </ul></div>
                                </div>
                                <div id="nav_menu-6" class="widget col-xs-6 col-sm-3 widget_nav_menu">
                                    <h3>Utilities</h3>
                                    <div class="menu-utilities-container"><ul id="menu-utilities" class="menu">
                                            <li id="menu-item-1891" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1891"><a href="#">Predictive Ajax search</a></li>
                                            <li id="menu-item-1892" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1892"><a href="#">Ajax filter in catalog page</a></li>
                                            <li id="menu-item-1893" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1893"><a href="#">Advanced reviews</a></li>
                                            <li id="menu-item-1894" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1894"><a href="#">Custom popup</a></li>
                                            <li id="menu-item-1895" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1895"><a href="#">Maintenance mode</a></li>
                                            <li id="menu-item-1896" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1896"><a href="#">Visual composer</a></li>
                                        </ul></div>
                                </div>                    </div>
                            <div class="clear"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END FOOTER -->
    <!-- START COPYRIGHT -->
    <div id="copyright">
        <div class="container">
            <div class="border">

                <div class="centered footer-extra-row col-sm-12">
                    <div class="credit_card c200"></div>
                    <div class="credit_card amex"></div>
                    <div class="credit_card cirrus"></div>
                    <div class="credit_card delta"></div>
                    <div class="credit_card discover"></div>
                    <div class="credit_card direct-debit"></div>
                    <div class="credit_card google"></div>
                    <div class="credit_card mastercard"></div>
                    <div class="credit_card maestro"></div>
                    <div class="credit_card moneybookers"></div>
                    <div class="credit_card moneygram"></div>
                    <div class="credit_card novus"></div>
                    <div class="credit_card paypal-2"></div>
                    <div class="credit_card plain"></div>
                    <div class="credit_card sage"></div>
                    <div class="credit_card solo"></div>
                    <div class="credit_card switch"></div>
                    <div class="credit_card visa"></div>
                    <div class="credit_card visa-electron"></div>
                    <div class="credit_card western-union"></div>
                    <div class="credit_card worldpay"></div>

                    <br><br>
                </div>
                <div class="centered">
                    <ul id="menu-copyright" class="level-1 clearfix">
                        <li id="menu-item-1875" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1677 current_page_item menu-item-children-0"><a href="index.html">Homepage</a></li>
                        <li id="menu-item-1889" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">About us</a></li>
                        <li id="menu-item-1890" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-children-0"><a href="#">Testimonials</a></li>
                        <li id="menu-item-1888" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-children-0"><a href="faqs/index.html">Faqs</a></li>
                    </ul>
                    <p>Copyright 2014 - Nielsen, the perfect e-commerce theme by YIThemes</p>


                </div>
            </div>

        </div>
    </div>
    <!-- END COPYRIGHT -->
</div>
</div>
<!-- END WRAPPER -->
<script>
    (function ($) {
        "use strict";
        // Author code here
        $('#menu-shop-by-category').find('ul.sub-menu li a').on('click', function (e) {
            e.preventDefault();
            var t = $(this);
            t.next('div.submenu').toggleClass('active');
            t.next('div.submenu').find('ul.sub-menu li a').off('click');
        });
    })(jQuery);
</script>
<script type="text/javascript" src="plugins/woocommerce/assets/js/js-cookie/js.cookie.min6b25.js?ver=2.1.4"></script>
<script type="text/javascript" src="assets/js/common1845.js?ver=4.9.6"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap1845.js?ver=4.9.6"></script>
<script type="text/javascript" src="assets/js/jquery.commonlibraries1845.js?ver=4.9.6"></script>
<script type="text/javascript" src="assets/js/internal1845.js?ver=4.9.6"></script>
</body>
<!-- END BODY -->
</html>