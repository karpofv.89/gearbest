<body id="home">
<!-- START WRAPPER -->
<div id="wrapper" class="clearfix">
    <!-- START HEADER -->
    <header id="header" class="clearfix skin1 sticky-header search-big">
        <div id="header-container" class="header-container">
            <div class="container-fluid col-lg-offset-2">
                <div class="header-wrapper clearfix">
                    <!-- HEADER MENU TRIGGER -->
                    <div id="mobile-menu-trigger" class="col-xs-2 visible-xs obile-menu-trigger">
                        <a href="#" data-effect="st-effect-4" class="fa fa-align-justify visible-xs"></a>
                    </div>
                    <!-- START LOGO -->
                    <div id="logo" class="no-tagline col-xs-9 col-sm-3 col-md-2 col-lg-2 text-center">
                        <a id="logo-img" href="#" title="Gearbest" class="logo-img">
                            <img src="assets/images/logo.png" title="Gearbest" alt="Gearbest">
                        </a>
                    </div>
                    <!-- END LOGO -->
                    <div id="header-search" class="header-search hidden-xs col-sm-8 col-lg-4">
                        <form role="search" method="get" id="yith-ajaxsearchform" action="http://live.yithemes.com/nielsen/">
                            <div>
                                <select class="siteSearch_cateText" id="search_categories" name="product_cat"><option value="" selected>Todas las categ...</option>
                                    <option value="accessories-type">Accessories</option>
                                    <option value="electronics">Electronics</option>
                                    <option value="interior-design">Interior Design</option>
                                </select>
                                <div class="nav-searchfield">
                                    <input type="search" value="" name="s" id="yith-s" class="yith-s" placeholder="Ingrese su busqueda ...">
                                </div>
                                <input type="submit" id="yith-searchsubmit" value="Search">
                            </div>
                        </form>
                        <div class="siteSearch_recom js-recomOuter">
                            <div class="siteSearch_recomList js-recomInner">
                                <a href="https://www.gearbest.com/huawei-_gear/" class="siteSearch_noWrapItem siteSearch_recomLink">Huawei</a>
                                <a href="https://www.gearbest.com/cutting-dies-c_12553/" class="siteSearch_noWrapItem siteSearch_recomLink">Cutting dies</a>
                                <a href="https://www.gearbest.com/xiaomi-mi-8-_gear/" class="siteSearch_noWrapItem siteSearch_recomLink">Mi 8</a>
                                <a href="https://www.gearbest.com/xiaomi-_gear/" class="siteSearch_noWrapItem siteSearch_recomLink">xiaomi</a>
                                <a href="https://www.gearbest.com/xiaomi-redmi-5-plus-_gear/" class="siteSearch_noWrapItem siteSearch_recomLink">xiaomi redmi 5 plus</a>
                                <a href="https://www.gearbest.com/Mi-Mix-2s-_gear/" class="siteSearch_noWrapItem siteSearch_recomLink">Mi Mix 2s</a>
                                <a href="https://www.gearbest.com/oneplus-_gear/" class="siteSearch_noWrapItem siteSearch_recomLink">Oneplus</a>
                            </div>
                        </div>
                    </div>
                    <div class="siteHeader_user hidden-xs col-sm-12 col-md-12 col-lg-4 text-center">
                        <div class="siteHeader_userItem">
                            <i class="fa fa-user">
                                <span class="unread_quantity js-unread-quantity"></span>
                            </i>
                            <div class="siteHeader_userInfo js-headerUserInfo">
                                <div class="siteHeader_userLogin">
                                    <p>Hi, rainbow</p>
                                </div>
                                <div class="siteHeader_userNoLogin">
                                    <a href="https://login.gearbest.com/m-users-a-sign.htm?type=1" class="siteHeader_userLinkLogin" rel="nofollow">Ingresa</a>
                                </div>
                            </div>
                            <div class="siteHeader_contain siteHeader_contain-move siteHeader_accountCon">
                                <ul class="siteHeader_userLogin">
                                    <li class="siteHeader_userInfoList"><a href="https://user.gearbest.com/index#/favor/goods" class="siteHeader_userLink" rel="nofollow">My Favorites</a></li>
                                    <li class="siteHeader_userInfoList"><a href="https://user.gearbest.com/index#/order/list?index=0" class="siteHeader_userLink" rel="nofollow">My Orders</a></li>
                                    <li class="siteHeader_userInfoList"><a href="https://support.gearbest.com/ticket/ticket/ticket-list?lang=en" class="siteHeader_userLink" rel="nofollow">My Tickets <span class="siteHeader_ticketNum js-siteHeader-ticketNum"></span></a></li>
                                    <li class="siteHeader_userInfoList"><a href="https://user.gearbest.com/index#/wallet/index" class="siteHeader_userLink" rel="nofollow">My GB Wallet</a></li>
                                    <li class="siteHeader_userInfoList"><a href="https://user.gearbest.com/index#/point" class="siteHeader_userLink" rel="nofollow">My Points</a></li>
                                    <li class="siteHeader_userInfoList"><a href="https://user.gearbest.com/index#/setting/info" class="siteHeader_userLink" rel="nofollow">My Profile</a></li>
                                    <li class="siteHeader_userInfoList"><a href="https://user.gearbest.com/index#/coupon" class="siteHeader_userLink" rel="nofollow">My Coupon</a></li>
                                    <li class="siteHeader_userInfoList"><a href="https://login.gearbest.com/m-users-a-logout.htm" class="siteHeader_userLink js-logout" rel="nofollow">Logout</a></li>
                                </ul>
                                <div class="siteHeader_userNoLogin">
                                    <p class="siteHeader_userNoLoginText">Register on GearBest: Earn 10 points</p>
                                    <a href="https://login.gearbest.com/m-users-a-register.htm?type=1" class="btn wide strong middle">Register</a>
                                </div>
                            </div>
                        </div>
                        <div class="siteHeader_userItem">
                            <a href="https://user.gearbest.com/index#myFavorites" rel="nofollow"><i class="fa fa-heart"></i></a>
                            <a href="https://user.gearbest.com/index#myFavorites" class="siteHeader_userInfo" rel="nofollow">
                                <span class="siteHeader_circleTag js-collectNum"></span>
                                <p class="siteHeader_userLinkLogin">Favoritos</p>
                            </a>
                        </div>
                        <div class="siteHeader_userItem js-relocate">
                            <a href="https://cart.gearbest.com/cart/index" rel="nofollow"><i class="fa fa-cart-plus"></i></a>
                            <a href="https://cart.gearbest.com/cart/index" class="siteHeader_userInfo" rel="nofollow">
                                <span class="siteHeader_circleTag js-cartNum"></span>
                                <p class="siteHeader_userLinkLogin">Carrito</p>
                            </a>
                            <div class="siteHeader_contain siteHeader_contain-move headerCart js-headerCart" style="right: initial;">
                                <div class="headerCart_emptyBox">
                                    <img src="https://css.gbtcdn.com/imagecache/gbw/img/site/cart@.png" alt="" class="headerCart_emptyImg">
                                    <p class="headerCart_emptyText">Tu carrito esta vacio. Compra!.</p>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- MOBILE SIDEBAR -->
                    <div class="mobile-sidebar hidden">
                        <div id="welcome-menu-login" class="nav">
                            <ul id="menu-welcome-login">
                                <li class="menu-item login-menu dropdown">
                                    <a href="my-account/index.html">Ingresa/Registrate</a>  
                                </li>
                            </ul>
                        </div>
                        <div class="nav whislist_nav">
                            <ul>
                                <li>
                                    <a href="javascript:void(0);">Favoritos</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">ventas rapidas</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">nuevo</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">mercado</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">comunidad</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- END MOBILE SIDEBAR -->
                </div>
            </div>
            <div class="clearfix container-fluid" style="background-color:#404040 ">
                <div id="header-search" class="header-search-menu col-lg-offset-2 col-xs-12">
                    <div class="shop-by-category border-line-2 nav vertical opened hidden-sm col-xs-12 col-lg-2">
                        <a href="#" class="list-trigger">ELIGE POR CATEGORÍA<i class="fa fa-angle-down"></i></a>
                    </div>
                    <!-- START NAVIGATION -->
                    <nav id="nav" role="navigation" class="nav header-nav hidden-xs col-sm-12 col-md-8">
                        <div class="level-1 clearfix">
                            <ul id="menu-main-navigation" class="menu">
                                <li id="menu-item-1718" class="bigmenu menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1677 current_page_item current-menu-ancestor current_page_ancestor menu-item-has-children menu-item-children-2">
                                    <a href="index.html">VENTAS RAPIDAS</a>
                                    <div class="submenu clearfix">
                                        <ul class="sub-menu clearfix">
                                            <li id="menu-item-1978" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-children-0">
                                                <a href="#">Gearbest</a>
                                                <a href="#">Gearbest</a>
                                                <a href="#">Gearbest</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li id="menu-item-1510" class="bigmenu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-2">
                                    <a href="#">NUEVO</a>
                                    <div class="submenu clearfix">

                                        <ul class="sub-menu clearfix">
                                            <li id="menu-item-1963" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-children-0">
                                                <a href="about-us/index.html">Recien llegado</a>
                                                <a href="about-us/index.html">Preventa</a>                                                    
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li id="menu-item-1461" class="bigmenu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-2">
                                    <a href="shop/index.html">MERCADO</a>
                                    <div class="submenu clearfix">
                                        <ul class="sub-menu clearfix">
                                            <li id="menu-item-1463" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-5">
                                                <a href="#">Explora</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li id="menu-item-1513" class="bigmenu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-2">
                                    <a href="#">Comunidad</a>
                                    <div class="submenu clearfix">

                                        <ul class="sub-menu clearfix">
                                            <li id="menu-item-1515" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                                <a href="#">Blog</a>
                                                <a href="#">Video</a>                                                    
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li id="menu-item-1513" class="bigmenu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-children-2">                                        
                                <li>
                                    <a class="siteHeader_botLinkBanner" href="https://www.gearbest.com/top-brands/brand/homtom.html"><img src="https://uidesign.gbtcdn.com/GB/images/banner/brand_logo/homtom.png"></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <!-- END NAVIGATION -->                        
                </div>
            </div>                
        </div>
    </header><!-- END HEADER -->
    <div class="slider-container clearfix">